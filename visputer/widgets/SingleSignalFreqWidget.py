from pyqtgraph.Qt import QtCore, QtGui

import numpy as np
import pyqtgraph as pg
from pyqtgraph.widgets.GraphicsView import GraphicsView

from pyqtgraph import GraphicsLayoutWidget

__all__ = ['SingleSignalFreqWidget']
class SingleSignalFreqWidget(GraphicsView):
    '''
    Single signal frequency analysis widget.
    '''

    def __init__(self, plotTitle="Signal frequency plot", plotName="SingleSignalFreq"):
        '''
        Constructor
        '''
        GraphicsView.__init__(self)
        
        self.layout = layout = QtGui.QGridLayout()
        self.setLayout(layout)
        
        self.plot = plot = pg.PlotWidget(title=plotTitle, name=plotName)
        plot.showGrid(x=True, y=True)
        plot.setLabel('left', "Frequency", units='Hz')
        plot.setLabel('bottom', "Time", units='s')
        layout.addWidget(plot, 0, 0)
        
        self.imageview = imv = pg.ImageItem()
        plot.addItem(imv)
        
        self.gradientHolder = gradientHolder = GraphicsLayoutWidget()
        self.gradient = gradient = pg.GradientEditorItem(orientation="right")
        gradientHolder.addItem(gradient)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        gradientHolder.setSizePolicy(sizePolicy)
        gradientHolder.setMaximumSize(QtCore.QSize(100, 16777215))
        layout.addWidget(gradientHolder, 0, 1)
        
        # set Lookup table
        updater = updateLUT(imv, gradient, 256)
        updater.run()
    
    def setData(self, data):
        self.data = data
        self.dataImage = self.makeFreqImage(data)
        self.imageview.setImage(self.dataImage)

    def makeFreqImage(self, data, windowSize = 400, windowStep = 100):
        dataLen = len(data)
        imageWidth = int((dataLen - windowSize + 1) / windowStep)
        imgDims = (imageWidth, windowSize/2+1)
        print imgDims
        image = np.zeros(imgDims)
        for step in range(imageWidth):
            pos = step * windowStep
            fftData = np.fft.rfft(data[pos:pos+windowSize])
            image[step,:] = abs(fftData[:])#0:windowSize])
        return image
    
class updateLUT(object):
    
    def __init__(self, imageview, gradient, lutSize):
        self.imageview = imageview
        self.gradient = gradient
        self.lutSize = lutSize
        
        self.gradient.sigGradientChanged.connect(self.makeConnector())
        
    def run(self):
        LUT = self.gradient.getLookupTable(self.lutSize)
        self.imageview.setLookupTable(LUT)

    def makeConnector(self):
        def connected():
            self.run()
        return connected
