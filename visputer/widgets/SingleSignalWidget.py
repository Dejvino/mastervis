from pyqtgraph.Qt import QtCore, QtGui

import pyqtgraph as pg
from pyqtgraph.widgets.GraphicsView import GraphicsView

from pyqtgraph import GraphicsLayoutWidget

__all__ = ['SingleSignalWidget']
class SingleSignalWidget(GraphicsView):
    '''
    Single signal widget.
    '''

    def __init__(self, plotTitle="Signal plot", plotName="SingleSignal"):
        '''
        Constructor
        '''
        GraphicsView.__init__(self)
        
        self.layout = layout = QtGui.QGridLayout()
        self.setLayout(layout)
        
        self.plot = plot = pg.PlotWidget(title=plotTitle, name=plotName)
        plot.showGrid(x=True, y=True)
        plot.setLabel('left', "Value", units='V')
        plot.setLabel('bottom', "Time", units='s')
        layout.addWidget(plot)
        
        self.plotPen = (255,255,0)
        self.curve = plot.plot([0], pen=self.plotPen) 
    
    def setData(self, data):
        self.data = data
        self.curve.setData(data)
