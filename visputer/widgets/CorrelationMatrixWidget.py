from pyqtgraph.Qt import QtCore, QtGui

import pyqtgraph as pg
from pyqtgraph.widgets.GraphicsView import GraphicsView

from pyqtgraph import GraphicsLayoutWidget

__all__ = ['CorrelationMatrixWidget']
class CorrelationMatrixWidget(GraphicsView):
    '''
    Correlation matrix widget.
    '''

    def __init__(self, plotTitle="Correlation matrix", plotName="CorrelationMatrix"):
        '''
        Constructor
        '''
        GraphicsView.__init__(self)
        
        self.layout = layout = QtGui.QGridLayout()
        self.setLayout(layout)
        
        self.plot = plot = pg.PlotWidget(title=plotTitle, name=plotName)
        plot.showGrid(x=True, y=True)
        plot.setLabel('left', "Signal", units=None)
        plot.setLabel('top', "Signal", units=None)
        layout.addWidget(plot, 0, 0)
        
        self.imageview = imv = pg.ImageItem()
        plot.addItem(imv)
        
        self.gradientHolder = gradientHolder = GraphicsLayoutWidget()
        self.gradient = gradient = pg.GradientEditorItem(orientation="right")
        gradientHolder.addItem(gradient)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        gradientHolder.setSizePolicy(sizePolicy)
        gradientHolder.setMaximumSize(QtCore.QSize(100, 16777215))
        layout.addWidget(gradientHolder, 0, 1)
        
        # set Lookup table
        updater = updateLUT(imv, gradient, 256)
        updater.run()
    
    def setData(self, data):
        self.imageview.setImage(data)
    
class updateLUT(object):
    
    def __init__(self, imageview, gradient, lutSize):
        self.imageview = imageview
        self.gradient = gradient
        self.lutSize = lutSize
        
        self.gradient.sigGradientChanged.connect(self.makeConnector())
        
    def run(self):
        LUT = self.gradient.getLookupTable(self.lutSize)
        self.imageview.setLookupTable(LUT)

    def makeConnector(self):
        def connected():
            self.run()
        return connected
    