'''
Created on Feb 28, 2013

@author: Dejvino
'''

import pickle

def save_time_series(time_series, filename):
    '''
    Writes the given time_series into a file.
    
    Internally the serialisation is handled by the pickle module.
    '''
    output = open(filename, 'wb')

    # Pickle using protocol 0.
    pickle.dump(time_series, output)
    
    output.close()


def load_time_series(filename):
    '''
    Loads a time-series from a given file.
    
    Intenrally the serialisation is handled by the pickle module.
    '''
    input = open(filename, 'rb')
    
    time_series = pickle.load(input)
    
    input.close()
    
    return time_series
