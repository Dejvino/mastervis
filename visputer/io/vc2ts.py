#!/usr/local/bin/python2.7
# encoding: utf-8
'''
visputer.vc2ts -- ValueContainer to TimeSeries

@author:     Dejvino
'''

import sys
import os

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter

import nitime
import nitime.timeseries as ts

import read_vc
import tsSaver
        
__all__ = []
__version__ = 0.1
__date__ = '2013-03-02'
__updated__ = '2013-03-02'

def load(input, verbose=0):
    """
    Loads a VC+VCM file as a TimeSeries object.
    
    This is the main function of this module.
    """
    vc = read_vc.readVC(input)
    vcm = read_vc.readVCM(input+'m')
    
    data = vc['data'] 
    
    sampleDist = float(vcm.get('Source', 'sampleInterval'))
    if vcm.has_section('Correlation'):
        windowStep = int(vcm.get('Correlation', 'windowStep'))
        if verbose > 0: 
            print("Correlation data detected.")
            print("Window step: %d" % (windowStep))
            print("Source sampling distance: %f" % (sampleDist))
        sampleDist *= windowStep
            
    if verbose > 0:
        print("Header file: " + str(vc['header'])) 
        print("Sampling distance: %f" % (sampleDist))
    
    if verbose > 0:
        print("Creating time-series.")
    series = ts.TimeSeries(data, sampling_interval=sampleDist)
    return series

class CLIError(Exception):
    '''Generic exception to raise and log different fatal errors.'''
    def __init__(self, msg):
        super(CLIError).__init__(type(self))
        self.msg = "E: %s" % msg
    def __str__(self):
        return self.msg
    def __unicode__(self):
        return self.msg

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''
    
    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = "..."#__import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by David Nemecek on %s.
  
USAGE
''' % (program_shortdesc, str(__date__))

    try:
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=RawDescriptionHelpFormatter)
        parser.add_argument("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %(default)s]")
        parser.add_argument('-V', '--version', action='version', version=program_version_message)
        parser.add_argument(dest="input", help="ValueContainer *.vc input file")
        parser.add_argument(dest="output", help="TimeSeries *.ts output file")
        
        # Process arguments
        args = parser.parse_args()
        
        input = args.input
        output = args.output
        verbose = args.verbose
        
        if verbose > 0:
            print("Verbose mode on")

        series = load(input, verbose)
        
        if verbose > 0:
            print("Saving file '%s'." % (output))
        tsSaver.save_time_series(series, output)
        
        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        if verbose > 0:
            import traceback
            traceback.print_exc(file=sys.stdout)
            print(e)
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help")
        return 2

if __name__ == "__main__":
    sys.exit(main())