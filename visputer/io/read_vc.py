'''
visputer.read_vc -- ValueContainer file format reader.
'''

import numpy as np

from struct import unpack

import ConfigParser

SIZEOF_FLOAT = 4

VALUECONTAINER_FILE_VERSION = 1
VALUECONTAINER_FILE_DATAOFFSET = 64


def readHeader(f):
    """
    Read the file header. Returns the following structure:
     | version -- File format version
     | streamsCount -- Number of streams in the data section
     | streamsLength -- Length of streams in the data section
    """

    header = {'version': 0, 'streamsCount': 0, 'streamsLength': 0}
    f.seek(0)
    data = f.read(3 * 4)
    header['version'] = unpack("i",data[0:4])[0]
    header['streamsCount'] = unpack("i",data[4:8])[0]
    header['streamsLength'] = unpack("i",data[8:12])[0]
    
    if VALUECONTAINER_FILE_VERSION != header['version']:
        raise ValueError("Invalid version number: " + str(header['version']))
     
    return header

def readData(f, header, index):
    """read data of a single channel"""
    
    streamLength = header['streamsLength']
    
    # seek stream start
    f.seek(VALUECONTAINER_FILE_DATAOFFSET + index * streamLength * SIZEOF_FLOAT)
    
    # read streamLength-times float
    raw = f.read(streamLength*SIZEOF_FLOAT)
    remains = streamLength*SIZEOF_FLOAT - len(raw)
    if remains > 0:
         raw = raw + ("\x00" * remains)
    data = list(unpack(str(streamLength)+'f', raw))
    return data

def readVC(filename):
    """
    Reads ValueContainer file and returns the following structure:
    header -- basic meta information
     | version -- File format version
     | streamsCount -- Number of streams in the data section
     | streamsLength -- Length of streams in the data section
    
    data -- Data matrix
     | Stream 0: Val0, Val1, ...
     | Stream 1: Val0, Val1, ...
     | ...
    """

    f = open(filename, 'rb');
    try:
        header = readHeader(f)
        
        data = np.zeros((header['streamsCount'], header['streamsLength']))
        for i in range(header['streamsCount']):
            data[i,:] = readData(f, header, i)
    finally:
        f.close()
        
    return {'header': header, 'data': data}

def readVCM(filename):
    """
    Reads a ValueContainer Metadata file.
    """
    config = ConfigParser.ConfigParser()
    config.read(filename)
    return config