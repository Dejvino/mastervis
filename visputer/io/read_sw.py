'''
visputer.read_sw -- ScopeWin file format reader.
'''

import numpy as np

from struct import unpack


MAXCHANNUMB = 128


def readIG(f):
    """ig -- data identification structure
       | softVersion -- max 20 chars
       | maxNumOfChannels -- maximal number of channels
    """

    ig = {'softVersion':'', 'maxNumOfChannels':0}
    data = unpack("20sh", f.read(20+2))
    ig['softVersion'] = data[0]
    ig['maxNumOfChannels'] = data[1]
    return ig

def readHG(f):
    """hg -- header common for all channels
      | numOfChannels -- number of channels
      | entry -- array associating channels with entries
      | size -- length of one channel in samples (same for all)
      | waveform -- type of measurement: 1=wave, 0=slow measurement
      | date -- string of data storage date
      | eventDetected -- bool, event detection by slow measurement
      | before -- number of points before measurement
      | eventTime -- time of the event
         | ti_min -- minutes
         | ti_hour -- hours
         | ti_hund -- hundreds of seconds
         | ti_sec -- seconds
      | comment -- user comment
    """

    hg = {'numOfChannels': 0, 'entry': [], 'size':0, 'waveform':0, 
          'date':'','eventDetected':False, 'before':0, 'eventTime':[], 
          'comment':''}
    data = f.read(2+MAXCHANNUMB*2)
    hg['numOfChannels'] = unpack("h",data[:2])[0]
    hg['entry'] = list(unpack(str(MAXCHANNUMB) + 'h',data[2:258]))

    data = unpack("ih12shi4B300s", f.read(4+2+12*1+2+4+4*1+300*1))
    hg['size'] = data[0]
    hg['waveform'] = data[1]
    hg['date'] = data[2]
    hg['eventDetected'] = bool(data[3])
    hg['before'] = data[4]
    hg['eventTime'] = list(data[5:9])
    hg['comment'] = data[9]
    return hg

def readFG(f):
    """fg header for particular entry (channel)
      | freq -- bool, 1 = frequency domain, 0 = time domain
      | name -- name of entry (channel)
      | si -- name of SI unit: K,V,Ohm,A, Celsius,, mmHg, AU, ...
      | unit -- magnitude of unit: 1 No U,  2 uU, 3 mU, 4 U, 5 kU, 6 MU
      | xunit -- time axes unit
      | xdelta -- time distance between two samples
    """

    fg = {'freq': 0, 'name': '', 'si': '', 'unit': 0, 'xunit': '',
          'xdelta': 0.0}
    data = unpack("h30s20sh20s", f.read(2+30+20+2+20))
    fg['freq'] = data[0]
    fg['name'] = data[1].rstrip("\0")
    fg['si'] = data[2].rstrip("\0")
    fg['unit'] = data[3]
    fg['xunit'] = data[4].rstrip("\0")

    fg['xdelta'] = unpack("f", f.read(4))
    return fg

def readData(f, length):
    """read data of a single channel"""
    data = list(unpack(str(length)+'f', f.read(length*4)))
    return data

def readSW(filename, hack=True):
    """Reads EEG data in ScopeWin format to a following structure:
    ig -- data identification structure
     | softVersion -- max 20 chars
     | maxNumOfChannels -- maximal number of channels
    
    hg -- header common for all channels
     | numOfChannels -- number of channels
     | entry -- array associating channels with entries
     | size -- length of one channel in samples (same for all)
     | waveform -- type of measurement: 1=wave, 0=slow measurement
     | date -- string of data storage date
     | eventDetected -- bool, event detection by slow measurement
     | before -- number of points before measurement
     | eventTime -- time of the event
        | ti_min -- minutes
        | ti_hour -- hours
        | ti_hund -- hundreds of seconds
        | ti_sec -- seconds
     | comment -- user comment
    
    fgs array of headers for particular entries (channels)
     | freq -- bool, 1 = frequency domain, 0 = time domain
     | name -- name of entry (channel)
     | si -- name of SI unit: K,V,Ohm,A, Celsius,, mmHg, AU, ...
     | unit -- magnitude of unit: 1 No U,  2 uU, 3 mU, 4 U, 5 kU, 6 MU
     | xunit -- time axes unit
     | xdelta -- time distance between two samples
    """

    f = open(filename, 'rb');
    try:
        ig = readIG(f)
        hg = readHG(f)

        if hack:
            endOfJunk = hg['numOfChannels'] * (78 + hg['size']*4)
            f.seek(-endOfJunk, 2)

        eegData = np.zeros((hg['size'], hg['numOfChannels']),dtype=np.float32)
        fgs = []
        for i in range(hg['numOfChannels']):
            fgs.append(readFG(f))
            eegData[:,i] = readData(f,hg['size'])
    finally:
        f.close()

    sw = {'ig':ig,'hg':hg, 'fgs':fgs, 'data':eegData}
    return sw
