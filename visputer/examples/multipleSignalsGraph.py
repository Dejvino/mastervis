# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg

from visputer.signalSimplification import simplifyDataMinMax
import visputer.io.vc2ts as vc2ts
from visputer.AsyncTask import AsyncTask

import exampleLoader

# create basic GUI
app = QtGui.QApplication([])
w = QtGui.QWidget()
layout = QtGui.QGridLayout()
w.setLayout(layout)

plot = pg.PlotWidget(title="Signals graph plot", name="SignalPlot")
plot.showGrid(x=True, y=True)
plot.setLabel('left', "Signal", units=None)
plot.setLabel('bottom', "Time", units='s')
layout.addWidget(plot)

spinner = pg.SpinBox(value=10, int=True, dec=True, minStep=1, step=1)
layout.addWidget(spinner)

# plot curve containers
curves = []
curveLines = []

# handle plots distance change
def updatePlots(ystep):
    global curveLines
    for (key, curve) in enumerate(curves):
        print "Updating curve %d." % (key)
        line = np.array(curveLines[key]['y'])
        curve.setData({'x': curveLines[key]['x'], 'y': line + key * ystep})

def spinnerChanged(sb):
    updatePlots(int(sb.value()))
spinner.sigValueChanged.connect(spinnerChanged)

# load data in a background thread
class LoadDataTask(AsyncTask):
    def __init__(self):
        AsyncTask.__init__(self)
        self.ts = None
        self.curveLines = []
        
    def preExecute(self):
        plot.setTitle("...loading...")
        
    def run(self):
        # load data
        self.ts = vc2ts.load(exampleLoader.VcInputFile, True)
        
        # simplify data
        data = self.ts.data
        for key in range(data.shape[0]-1):
            self.curveLines.append(simplifyDataMinMax(data[key,:], 10))
        
    def postExecute(self):
        global curves
        global curveLines
        curveLines = self.curveLines
        for key in range(len(curveLines)):
            linePen = ((np.math.sin(key * 7.0)+1.0) * 100 + 55,
                       (np.math.sin(key * 17.0)+1.0) * 100 + 55,
                       (np.math.sin(key * 11.0)+1.0) * 100 + 55)
            curve = plot.plot(np.zeros(10), pen=linePen)
            curves.append(curve)
        
        plot.setTitle("Multiple signals graph")
        
        updatePlots(1000)

thread = LoadDataTask()
thread.start()

w.show()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

