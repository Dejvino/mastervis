'''
Loader of example-related things. No need for this script in your release code.
'''

import os
dir = os.path.realpath(os.path.dirname(__file__))

# synthetic test files
VcInputFile = os.path.realpath(dir + "/../../../data/example.vc")
CorrelFile = os.path.realpath(dir + "/../../../data/example.corel.vc")