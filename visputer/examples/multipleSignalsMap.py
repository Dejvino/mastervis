# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore, USE_PYSIDE

import numpy as np
import pyqtgraph as pg

from visputer.signalSimplification import simplifyDataImage, simplifyDataMinMax
import visputer.io.vc2ts as vc2ts
from visputer.AsyncTask import AsyncTask

import exampleLoader

from Ui_SignalsView import Ui_Form as Ui_SignalsView

basicFactor = 100
overviewFactor = 10

class MultipleSignalsMapWindow(QtCore.QObject):
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        
        self.win = win = pg.GraphicsWindow()
        win.resize(800,800)
        
        self.ui = ui = Ui_SignalsView()
        ui.setupUi(win)
        
        self.plot = plot = ui.plot
        self.imv = imv = pg.ImageItem()
        plot.addItem(imv)
        
        self.gradient = gradient = pg.GradientEditorItem(orientation="right")
        ui.gradient.addItem(gradient)
        
        self.overview = overview = pg.ImageItem()
        ui.overview.addItem(overview)
        
        self.lr = lr = pg.LinearRegionItem([0,100])
        lr.setZValue(-10)
        ui.overview.addItem(lr)
        
        # set Lookup table
        lutSize = 256
        def updateLUT():
            LUT = gradient.getLookupTable(lutSize)
            imv.setLookupTable(LUT)
            overview.setLookupTable(LUT)
        gradient.sigGradientChanged.connect(updateLUT)
        updateLUT()
        
        # handle view move
        def updateView():
            lr.setZValue(10)
            minX, maxX = lr.getRegion()
            plot.setXRange(minX*overviewFactor, maxX*overviewFactor, padding=0)    
        
        lr.sigRegionChanged.connect(updateView)
        
        def updateRegion(window, viewRange):
            if not isinstance(viewRange, list):
                return
            rgn = viewRange[0]
            region = [rgn[0]/overviewFactor, rgn[1]/overviewFactor]
            lr.setRegion(region)
            self.emit(QtCore.SIGNAL("updateRegion"), region)
        
        plot.sigRangeChanged.connect(updateRegion)
        
        lr.setRegion([0, 100])

        win.show()

# load data in a background thread
class LoadDataTask(AsyncTask):
    def __init__(self, win):
        AsyncTask.__init__(self)
        self.simpleData = None
        self.simplerData = None
        self.win = win
        
    def preExecute(self):
        self.setStatus("...loading...")
        
    def run(self):
        # load data
        ts = vc2ts.load(exampleLoader.VcInputFile)
        
        self.emit(QtCore.SIGNAL("setStatus(str)"), ("...processing..."))
        
        # simplify the data 
        self.simpleData = simplifyDataImage(ts.data, simplifyDataMinMax, basicFactor)
        if overviewFactor <= 1:
            self.simplerData = self.simpleData
        else:
            self.simplerData = simplifyDataImage(self.simpleData, simplifyDataMinMax, overviewFactor)
        
    def postExecute(self):
        win = self.win
        win.imv.setImage(np.asmatrix(self.simpleData).T)
        win.overview.setImage(np.asmatrix(self.simplerData).T)
        win.ui.plot.autoRange()
        win.ui.overview.autoRange()
        self.setStatus("Multiple signals color map")
        
    def setStatus(self, text):
        self.win.plot.setTitle(text)


## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    app = QtGui.QApplication([])
    win = MultipleSignalsMapWindow()
    thread = LoadDataTask(win)
    thread.start()
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
