# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

import visputer.io.vc2ts as vc2ts

import exampleLoader

app = QtGui.QApplication([])

win = pg.GraphicsWindow(title="Simple correlation viewer")
win.resize(1000,600)

ts = vc2ts.load(exampleLoader.CorrelFile)

p1 = win.addPlot(title="Correlation plot :: 2 x 5", name="CorrelationPlot")
p1.showGrid(x=True, y=True)
p1.setLabel('left', "Value", units=None)
p1.setLabel('bottom', "Time", units='s')
p1.plot(ts.data[5 + 16,:], pen=(155,255,0))

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

