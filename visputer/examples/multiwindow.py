# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore

from visputer.widgets.CorrelationMatrixWidget import *
import visputer.io.vc2ts as vc2ts
import visputer.correlation as correl

import multipleSignalsMap
import simpleCorrelationMatrix

# create GUI
app = QtGui.QApplication([])

# Window 1 - signals map view
win1 = multipleSignalsMap.MultipleSignalsMapWindow()

# Window 2 - correlation matrix
win2 = simpleCorrelationMatrix.CorrelationMatrixWindow()

# connect windows
correlData = []
def updateMatrixData(region):
    global correlData
    pos = region[0]
    # TODO: convert signal time to correlation time
    win2.cormat.setData(correl.makeMatrixFromStreams(correlData[:,pos]))
    win2.cormat.plot.autoRange()
    
win1.connect(win1, QtCore.SIGNAL("updateRegion"), updateMatrixData)

# load data
thread1 = multipleSignalsMap.LoadDataTask(win1)
thread1.start()

class CorrelThread(simpleCorrelationMatrix.LoadDataTask):
    def postExecute(self):
        global correlData
        correlData = self.ts.data
        updateMatrixData([0,0])
        self.setStatus("Correlation matrix")
         
thread2 = CorrelThread(win2)
thread2.start()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

