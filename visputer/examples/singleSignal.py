# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore

from visputer.widgets.SingleSignalWidget import *
import visputer.io.vc2ts as vc2ts
from visputer.AsyncTask import AsyncTask

import exampleLoader

# create GUI
app = QtGui.QApplication([])
w = QtGui.QWidget()
layout = QtGui.QGridLayout()
w.setLayout(layout)

plot = SingleSignalWidget()
layout.addWidget(plot)

w.show()

# load data in a background thread
class LoadDataTask(AsyncTask):
    def __init__(self):
        AsyncTask.__init__(self)
        self.ts = None
        
    def preExecute(self):
        plot.plot.setTitle("...loading...")
        
    def run(self):
        self.ts = vc2ts.load(exampleLoader.VcInputFile)
    
    def postExecute(self):
        plot.setData(self.ts.data[1,:])
        plot.plot.setTitle("Signal plot")

thread = LoadDataTask()
thread.start()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

