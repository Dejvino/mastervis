# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore

from visputer.widgets.CorrelationMatrixWidget import *
import visputer.io.vc2ts as vc2ts
import visputer.correlation as correl
from visputer.AsyncTask import AsyncTask

import exampleLoader

class CorrelationMatrixWindow(QtCore.QObject):
    
    def __init__(self):
        QtCore.QObject.__init__(self)
        
        self.w = w = QtGui.QWidget()
        self.layout = layout = QtGui.QGridLayout()
        w.setLayout(layout)
        
        self.cormat = cormat = CorrelationMatrixWidget()
        layout.addWidget(cormat)
        
        w.show()
       
# load data in a background thread
class LoadDataTask(AsyncTask):
    def __init__(self, win):
        AsyncTask.__init__(self)
        self.win = win
        self.ts = None
        
    def preExecute(self):
        self.setStatus("...loading...")
        
    def run(self):
        self.ts = vc2ts.load(exampleLoader.CorrelFile)
        
    def postExecute(self):
        win = self.win
        win.cormat.setData(correl.makeMatrixFromStreams(self.ts.data[:,0]))
        win.cormat.plot.autoRange()
        self.setStatus("Correlation matrix")
        
    def setStatus(self, text):
        self.win.cormat.plot.setTitle(text)

if __name__ == '__main__':
    app = QtGui.QApplication([])
    win = CorrelationMatrixWindow()
    thread = LoadDataTask(win)
    thread.start()
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

