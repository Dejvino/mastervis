# -*- coding: utf-8 -*-

from pyqtgraph.Qt import QtGui, QtCore

from visputer.widgets.SingleSignalFreqWidget import *
import visputer.io.vc2ts as vc2ts

import exampleLoader

# create GUI
app = QtGui.QApplication([])
w = QtGui.QWidget()
layout = QtGui.QGridLayout()
w.setLayout(layout)

plot = SingleSignalFreqWidget()
layout.addWidget(plot)

# load data
ts = vc2ts.load(exampleLoader.VcInputFile)

plot.setData(ts.data[1,:])

w.show()

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

