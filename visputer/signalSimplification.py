# -*- coding: utf-8 -*-

def simplifyDataNth(data, factor):
    dataSimpleY = []
    dataSimpleX = []
    for index, value in enumerate(data):
        if index % factor != 0:
            continue
        dataSimpleY.append(value)
        dataSimpleX.append(index)
    return {'x': dataSimpleX, 'y': dataSimpleY}
    
def simplifyDataMinMax(data, factor):
    dataSimpleY = []
    dataSimpleX = []
    sMin = 0
    sMax = 0
    iMin = 0
    iMax = 0
    windowSize = factor * 2
    for index, value in enumerate(data):
        if index % windowSize != 0:
            if sMin > value:
                sMin = value
                iMin = index
            if sMax < value:
                sMax = value
                iMax = index
            continue
        if iMin <= iMax:
            dataSimpleY.append(sMin)
            dataSimpleY.append(sMax)
        else:
            dataSimpleY.append(sMax)
            dataSimpleY.append(sMin)
        dataSimpleX.append(index - windowSize/2)
        dataSimpleX.append(index)
        sMin = data[index+1]
        sMax = data[index+1]
        iMin = index+1
        iMax = index+1
    return {'x': dataSimpleX, 'y': dataSimpleY}

def simplifyDataImage(data, simplifyFunc, factor):
    newData = []
    for line in data:
        newData.append(simplifyFunc(line, factor)['y'])
    return newData
