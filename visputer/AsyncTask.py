'''
Asynchronously executed task.
'''

import PyQt4 as qt
import PyQt4.QtCore as QtCore

class AsyncTask(qt.QtCore.QThread):
    '''
    AsyncTask -- asynchronously execute code with pre- and post-execution
    functions.
    '''

    def __init__(self):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
        
        # connect handled signals
        self.connect(self, QtCore.SIGNAL('executed()'), self.preExecute)
        self.connect(self, QtCore.SIGNAL('finished()'), self.postExecute)
        
    def start(self):
        self.emit(QtCore.SIGNAL("executed()"))
        QtCore.QThread.start(self)
        
    def preExecute(self):
        '''
        Pre-execute to be called before the execution starts.
        SLOT
        '''
        pass
        
    def postExecute(self):
        '''
        Post-execute to be called after the execution stops.
        SLOT
        '''
        pass
    