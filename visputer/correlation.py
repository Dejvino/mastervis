'''
Correlation tools.
'''

import math
import numpy as np

def makeMatrixFromStreams(input):
    """ Creates a matrix from a "flat" array. """
    elementsCount = len(input)
    streamsCount = int(math.sqrt(elementsCount))
    
    if streamsCount*streamsCount != elementsCount:
        raise ValueError("Invalid matrix size %d, must have square root." % (elementsCount))
    
    matrix = np.zeros((streamsCount, streamsCount))
    for x in range(streamsCount):
        for y in range(streamsCount):
            index = x + y*streamsCount
            if x == y:
                matrix[x,y] = 1.0
            else:
                matrix[x,y] = input[index]
                if matrix[x,y] > 1.0:
                    matrix[x,y] = 1.0
    
    #chunked = chunkArray(input, streamsCount)
    #print chunked
    
    #matrix = np.matrix(chunkArray(input, streamsCount))
    #matrix = np.matrix([[1,2,3], [1,2,2], [0,1,1]])
    
    #matrix[:,:] = 0
    #for i in range(streamsCount):
    #    matrix[i,i] = 1.0
    
    #print matrix
    return matrix 

def chunkArray(l, n):
    """ Chunks the input list _l_ into _n_ sub-lists. """
    return [l[i:i+n] for i in range(0, len(l), n)]